﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TreeLabLib
{
    public class BinaryTree<KeyType, ValueType>
           where KeyType : IComparable<KeyType>
    {
        
        public class Node
        {
            Node[] child = new Node[2] { null, null };
            enum Child { Left = 0, Right = 1 };
            public KeyType Key;
            public ValueType NodeValue;

            public Node(KeyType key, ValueType nodeValue)
            {
                Key = key;
                NodeValue = nodeValue;
            }

            public Node LeftChild
            {
                get
                {
                    return child[(int)Child.Left];
                }
                set
                {
                    child[(int)Child.Left] = value;
                }
            }

            public Node RightChild
            {
                get
                {
                    return child[(int)Child.Right];
                }
                set
                {
                    child[(int)Child.Right] = value;
                }

            }

            // Rotate functions for Nodes
            public Node rotateLeft(Node thisNode)
            {
                // Create a Node reference to thisNode's RightChild
                Node x = thisNode.RightChild;

                // thisNode's RightChild is now equal to its current left child
                thisNode.RightChild = x.LeftChild;

                // thisNode is now Node x's right child
                x.RightChild = thisNode;

                return x;
            }

            public Node rotateRight(Node thisNode)
            {
                // Same as above but for rotating right
                Node x = thisNode.LeftChild;
                thisNode.LeftChild = x.RightChild;
                x.RightChild = thisNode;
                return x;
            }


            public static void Insert(Node node, KeyType key,
                ValueType value)
            {
                // if the inserted key is less than the
                // nodes key.
                if (key.CompareTo(node.Key) < 0)
                {
                    if (node.LeftChild == null)
                    {
                        node.LeftChild = new Node(key, value);
                    }
                    else
                    {
                        // insert the new node into the left subtree.
                        Insert(node.LeftChild, key, value);
                    }
                }
                else
                {
                    if (key.CompareTo(node.Key) > 0)
                    {
                        if (node.RightChild == null)
                        {
                            node.RightChild = new Node(key, value);
                        }
                        else
                        {
                            // insert the new node into the left subtree.
                            Insert(node.RightChild, key, value);
                        }
                    }
                    else
                    {
                        // The keys are equal - replace the old value
                        node.NodeValue = value;
                    }
                }
            }

            public Node this[int childNumber]
            {
                get
                {
                    return child[childNumber];
                }
                set
                {
                    child[childNumber] = value;
                }

            }

            public static void InOrderTraversal (Node root, List<Tuple<KeyType, ValueType>> preOrderList)
            {
                //// Node.InOrderReversal for recursively calling the function

                // Recursively fill array with the values in the tree
                // using Node.InOrderTraversal if there is a Root
                if (root != null)
                {
                    // Visit the left subtree
                    InOrderTraversal(root.LeftChild, preOrderList);

                    // Visit the root
                    preOrderList.Add(new Tuple<KeyType, ValueType>(root.Key, root.NodeValue));

                    // Visit the right subtree
                    InOrderTraversal(root.RightChild, preOrderList);
                }
            }

            public static Node InsertAtRoot(Node node, KeyType key, ValueType value)
            {

            }
        }


        // BT Insert function.
        public void Insert(KeyType key,
            ValueType value)
        {
            // Check to see if the root is null
            if (Root == null)
            {
                Root = new Node(key, value);
            }
            else
            {
                // Call the node insert function.
                Node.Insert(Root, key, value);
            }
        }

        // BT Insert at root
        public void InsertAtRoot(KeyType key, ValueType value)
        {
            // Insert the node, then rotate it up the tree

            if (Root == null)
            {
                Insert(key, value);
            }
            else
            {
                Node.InsertAtRoot(Root, key, value);
            }


        }

        public bool IsEmpty()
        {
            return Root == null;
        }

        public int getNodeCount(Node node)
        {
            // Returns total number of nodes in the tree

            // Starts at 0
            int returnValue = 0;

            // If root has no children, set returnValue to 1
            if (node.LeftChild == null && node.RightChild == null)
                returnValue = 1;

            // Else, recursively determine the node counts for both left and right
            // sub trees
            else
            {
                returnValue += getNodeCount(node.LeftChild);
                returnValue += getNodeCount(node.RightChild);
            }

            return returnValue;
        }

        public int Height()
        {
            return Height(Root);
        }

        public IEnumerable<Tuple<KeyType, ValueType>> InOrderTraversal()
        {
            // Traverses left child, then root, then right child

            // Create a list of tuples
            List<Tuple<KeyType, ValueType>> inOrderList =
                new List<Tuple<KeyType, ValueType>>();

            if (Root != null)
            {
                Node.InOrderTraversal(Root, inOrderList);
            }

            return inOrderList;
            
        }

        private int Height(Node node)
        {
            // If node is null return -1
            if (node == null)
                return -1;

            // Variables for the height of left and right sub trees
            int leftHeight = Height(node.LeftChild);
            int rightHeight = Height(node.RightChild);

            // For each sub tree, if there is a child on either side,
            // height will return 0, otherwise, height will return -1

            // Return the greater of the two heights (plus 1) 
            // The 1 offsets the -1 and 0, so nodes with children will
            // return 1, those without return 0
            if (leftHeight > rightHeight)
                return leftHeight + 1;
            else
                return rightHeight + 1;
        }


        public Node Root = null;

        private KeyType Key { get; set; }
        private ValueType NodeValue { get; set; }

    }

}
